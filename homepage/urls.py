from django.urls import path
from .views import index, experience

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('experience/', experience, name='exp'),
    # dilanjutkan ...
]